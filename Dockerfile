FROM golang:1.17-alpine3.14
COPY . /src
WORKDIR /src
RUN go build

FROM alpine:3.14
COPY --from=0 /src/mailthulu /bin/mailthulu

CMD /bin/mailthulu
