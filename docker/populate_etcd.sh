#!/bin/bash

etcd_connect () {
  ETCDCTL_API=3 etcdctl --endpoints\
    http://localhost:2079,http://localhost:2179,http://localhost:2279\
    $@
}

etcd_connect put /mailthulu/forwarders/test@resuni.com mailthulu@1secmail.com
etcd_connect get / --prefix
