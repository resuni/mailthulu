#!/bin/bash

docker build -t registry.gitlab.com/resuni/mailthulu:latest .
docker build -t registry.gitlab.com/resuni/mailthulu/mailthulu-postfix:latest docker/postfix
docker push registry.gitlab.com/resuni/mailthulu
docker push registry.gitlab.com/resuni/mailthulu/mailthulu-postfix
