package main

import (
	"log"
	"time"

	"github.com/spf13/viper"
	"go.etcd.io/etcd/client/v3"

	"gitlab.com/resuni/mailthulu/bridge"
)

func main() {

	// Read config file with Viper.
	viper.SetConfigName("mailthulu.conf")
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/mailthulu/")

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal("Error reading config file: %w \n", err)
	}

	// Create etcd client.
	timeout := time.Duration(viper.GetInt("etcd.timeout")) * time.Second
	etcd, err := clientv3.New(clientv3.Config{
		Endpoints: viper.GetStringSlice("etcd.endpoints"),
		DialTimeout: timeout,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer etcd.Close()

	// Start postfix->etcd bridge.
	bridge.Start(etcd)

}
