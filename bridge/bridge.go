// Package bridge provides a TCP interface that postfix queries to retrieve
// forwarders from etcd.
package bridge

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/spf13/viper"
	"go.etcd.io/etcd/client/v3"
)

// Start TCP listener.
func Start(etcd *clientv3.Client) {

	// Define TCP server config options from Viper.
	host := viper.GetString("bridge.host")
	port := viper.GetString("bridge.port")

	// Listen for incoming connections.
	l, err := net.Listen ("tcp", host + ":" + port)
	if err != nil {
		log.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	defer l.Close()

	log.Println("Listening on " + host + ":" + port)
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new go routine.
		go handleRequest(etcd, conn)
	}
}

// Function handles incoming requests.
func handleRequest(etcd *clientv3.Client, conn net.Conn) {

	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)

	// Read the incoming connection into the buffer.
	_, err := conn.Read(buf)
	if err != nil {
		log.Printf("Bridge: Error reading: %s", err.Error())
	}

	// Convert request from byte slice to string.
	rawRequest := string(buf)

	// Log request.
	log.Printf("Bridge received: %s", rawRequest)

	// Parse request from postfix. Extract text before
	// newline, then parse space-separated fields.
	request := strings.Fields(strings.Split(rawRequest, "\n")[0])

	// Validate TCP request.
	validateResult := validateRequest(request)
	if validateResult != "OK" {
		conn.Write([]byte(validateResult))
		conn.Close()
		log.Printf("Bridge responded: %s", validateResult)
		return
	}

	// Look up forwarder destination in etcd.
	email, err := emailLookup(etcd, request[1])

	// Return error if etcd lookup fails.
	if err != nil {
		response := "400 etcd lookup error. See log for details.\n"
		conn.Write([]byte(response))
		conn.Close()
		log.Println(err)
		log.Printf("Bridge responded: %s", response)
		return
	}

	// Return 500 status if forwarder is not found.
	if email == "500" {
		response := "500 Forwarder not found.\n"
		conn.Write([]byte(response))
		conn.Close()
		log.Printf("Bridge responded: %s", response)
		return
	}

	// No failure conditions are met, so return the
	// URL-encoded destination email address.
	response := fmt.Sprintf("200 %s\n", url.QueryEscape(email))
	conn.Write([]byte(response))
	conn.Close()
	log.Printf("Bridge responded: %s", response)
	return

}

// Function looks up a forwarder in etcd and returns a destination email
// address. If a destination email address is not found, "500" is returned.
func emailLookup(etcd *clientv3.Client, address string) (string, error) {

	// Construct etcd path to email address.	
	etcdPath := fmt.Sprintf("/mailthulu/forwarders/%s", address)

	// Make request.
	timeout := time.Duration(viper.GetInt("etcd.timeout")) * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	resp, err := etcd.Get(ctx, etcdPath)
	cancel()
	if err != nil {
		return "", err
	}

	// Return "500" if response values count is 0.
	if resp.Count == 0 {
		return "500", nil
	}

	return string(resp.Kvs[0].Value), nil

}

// Function confirms the TCP request is in the correct format.
func validateRequest(request []string) string {

	// Confirm request contains two fields.
	if len(request) != 2 {
		return "400 Request must contain 2 fields.\n"
	}

	// Confirm request starts with "get".
	if request[0] != "get" {
		return fmt.Sprintf("400 Invalid command: '%s'\n", request[0])
	}

	return "OK"

}
